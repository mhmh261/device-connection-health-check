# Device Connection Health Check

## Table of Contents

- [Overview](#overview)
- [Features](#features)
- [Requirements](#requirements)
- [Supported Device Types](#supported-device-types)
- [Test Environment](#test-environment)
- [How to Run](#how-to-run)
- [Additional Information](#additional-information)

## Overview

This folder contains the Device Connection Health Check workflow, which is depicted in the following flowchart.
<!-- ![workflow shot](./img/basic_Workflow.png) -->
<table><tr><td>
  <img src="./img/basic_Workflow.png" alt="workflow shot" width="400px">
</td></tr></table>

**Legend**

| Label | Description |
| ------ | ------ |
| **A** | Wait for a number of seconds (#ofSeconds) before attempting to connect/reconnect device. |
| **Connect** | Attempt connection to the device and evaluate response. |
| **B** | If connection is unsuccessful, try reconnecting a number of times (#ofTimes). |
| **C** | If connection is successful, try connecting to the device for #ofSeconds.|

The workflow that is built connects to a device and attempts to connect for a certain #ofTimes to check stability. If the device is not alive, it will attempt to connect for a certain #ofTimes, after which it ends with a "fail" output. Output is "success" if the device connects successfully. Inputs are validated and produce a "bad input" error in case of wrong input.

<!-- ![workflow shot](./img/wf_shot.png) -->
<table><tr><td>
  <img src="./img/wf_shot.png" alt="workflow shot" width="800px">
</td></tr></table>

This is how it relates to the flowchart:
<!-- ![workflow shot](./img/wf_labels.png) -->
<table><tr><td>
  <img src="./img/wf_labels.png" alt="workflow shot" width="600px">
</td></tr></table>

This workflow is responsible for:

1. Adding a delay for #ofSeconds before connecting to the device (before each retry).
2. Attempting to connect to the device and getting a response (isAlive: true/false).
3. If device is not alive, retry step 2 for #ofTimes.
4. If device is alive, retry connecting consecutively with it #ofTimes (#retries).
5. If step 4 fails during this duration, go back to step 3. The #retries for step 4 is reset here to 0 (reset consecutive counter). 

## Features

Features of this artifact include:

* Auto-checks for input formatting errors.
* Connects to the device and determines if the device is alive.
* Waits for a certain amount of time before trying to establish a connection again.
* Retries only for a certain number of times (numberOfRetries).
* If connection to the device is successful, attempts for n - 1 more successful connections to the device (numberOfRetriesStability).

## Requirements

The following job variables are required to start the workflow.

| Job Variable | Description |
| ------ | ------ |
| device | Name of device.|
| pingDelay | Seconds to wait before trying to establish connection with the device again.|
| numberOfRetries | Maximum number of times to attempt for a successful connection with the device.|
| numberOfRetriesStability | The number requirement for consecutive successful connections (to validate interface stability).|

## Supported Device Types

Devices residing on any adapter that implement device-broker (Ex. NSO, Ansible) are supported. One may use any device that shows up in Configuration Manager. 

## Test Environment

* IAP version 2022.1

## How to Run

Running the device connection health check is meant to be called from parent workflows during device upgrade or any other maintenance that require verification of connection stability. To test this workflow in stand-alone mode:

1. Navigate to Workflow Builder.
2. Click the play button next to the connection health check workflow.
3. Supply the required inputs.
4. Click start.  

## Additional Information
Please use your Itential Customer Success account if you need support when using this artifact.
